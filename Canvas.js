Math.TAU = Math.PI * 2

console.call = function() {
  console.log('ok wtf?')
  Array.prototype.unshift.call(arguments, arguments.callee)
  console.log(arguments) }

// https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
// http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
// http://math.stackexchange.com/questions/111045/given-two-parallel-line-segments-how-do-i-tell-if-and-where-they-overlap
// https://github.com/bit101/CodingMath/blob/master/episode33/main_interactive.js
function segmentIntersect(p0, p1, p2, p3) {
  var A1 = p1.y - p0.y
  var B1 = p0.x - p1.x
  var C1 = A1 * p0.x + B1 * p0.y
  var A2 = p3.y - p2.y
  var B2 = p2.x - p3.x
  var C2 = A2 * p2.x + B2 * p2.y
  var denominator = A1 * B2 - A2 * B1

  if(denominator == 0) {
    return null }

  var intersectX = (B2 * C1 - B1 * C2) / denominator
  var intersectY = (A1 * C2 - A2 * C1) / denominator
  var rx0 = (intersectX - p0.x) / (p1.x - p0.x)
  var ry0 = (intersectY - p0.y) / (p1.y - p0.y)
  var rx1 = (intersectX - p2.x) / (p3.x - p2.x)
  var ry1 = (intersectY - p2.y) / (p3.y - p2.y)

  if (((rx0 >= 0 && rx0 <= 1) || (ry0 >= 0 && ry0 <= 1)) && 
  ((rx1 >= 0 && rx1 <= 1) || (ry1 >= 0 && ry1 <= 1))) {
    return {
    x: intersectX,
    y: intersectY } }
  else {
    return null } }

function Canvas() {
  var c = function() {
    c._function.apply(c, arguments) }

  c._function = function(a, b) {
    console.log('_function', a, b) }

  c.auto = function() {
    var r = c.element.getBoundingClientRect()
    c.element.style.width = '100%'
    c.element.style.height = c.window.innerHeight * .8 + 'px'
    c.canvasToRes()
    c.resToCanvas() }

  c.canvasToRes = function() {
    var r = c.element.getBoundingClientRect()
    c.element.width = r.width
    c.element.height = r.height }

  c.disablePipes = function(thing) {
    if (typeof thing._update === 'function')
      c.pipes.update.splice(c.pipes.update.indexOf(thing._update, 1))

    if (typeof thing._draw === 'function')
      c.pipes.draw.splice(c.pipes.draw.indexOf(thing._draw, 1))

    if (typeof thing._hud === 'function')
      c.pipes.hud.splice(c.pipes.hud.indexOf(thing._hud, 1)) }

  c.clear = function(color) {
    var r = c.element.getBoundingClientRect()
    c.context2d.fillRect(0, 0, r.width, r.height, color) }

  c.create = function(type, thing, whatever) {
    arguments.callee._count = arguments.callee._count || 0

    var givenType = type
    var args = []

    if (typeof Canvas.types[type] !== 'function') {
      //console.warn('c.create: undefined constructor:', type)
      type = 'thing' }

    if (typeof thing === 'undefined') {
      //console.warn('c.create: no thing (it\'s OK though)')
      thing = {}
      args.push(thing) }

    thing._type = type
    thing._givenType = givenType

    if (typeof thing._id === 'undefined')
      thing._id = 'thing-' + c.nextId++

    for (var i = 1 ; i < arguments.length ; i++)
      args.push(arguments[i])

    var thing = Canvas.types[type].apply(c, args)
    c.things[thing._id] = thing
    c.enablePipes(thing)
    return thing }

  c.enablePipes = function(thing) {
    if (typeof thing._update === 'function' && c.pipes.update.indexOf(thing._update, 1) < 0) {
      thing._update.context = thing
      c.pipes.update.push(thing._update) }

    if (typeof thing._draw === 'function' && c.pipes.draw.indexOf(thing._draw, 1) < 0) {
      thing._draw.context = thing
      c.pipes.draw.push(thing._draw) }

    if (typeof thing._hud === 'function' && c.pipes.hud.indexOf(thing._hud, 1) < 0) {
      thing._hud.context = thing
      c.pipes.hud.push(thing._hud) } }

  c.feedback = function() {
    console.info('feedback!', arguments) }

  c.fit = function() {
    var r = c.element.getBoundingClientRect()
    c.element.style.width = '100%'
    c.element.style.height = c.window.innerHeight * .8 + 'px' }

  c.frame = function(count, time) {
    c.pipe('update', arguments)
    c.pipe('draw', arguments)
    c.pipe('hud', arguments) }

  c.frameCount = -1

  c.getKnob = function(title, object, field, inputFilter, outputFilter) {
    var e = document.createElement('section')
    e.style.background = 'rgba(255, 255, 255, .25)'
    //e.style.padding = '0.15rem'
    e.style.margin = '0.15rem'
    e.style.color = 'rgba(255, 255, 255, .75)'
    e.style.display = 'inline-block'
    //e.style.border = '1px solid white'
    e.style.padding = '0 0.5rem'
    //e.style.margin = '0.5rem'
    e.style['border-radius'] = '0.25rem'

    var t = document.createElement('span')
    t.style.padding = '0.1rem 0.25rem'
    t.style.fontSize = '0.9rem'
    e.appendChild(t)
    t.innerText = title

    var i = document.createElement('input')
    i.style.background = 'rgba(0, 0, 0, .5)'
    i.style.color = 'rgba(255, 255, 255, 1)'
    i.style.width = '4rem'
    i.style.padding = '0.1rem 0.25rem'
    i.style.border = 'none'
    i.style.fontFamily = 'monospace'
    i.value = object[field]
    e.appendChild(i)

    if (field === 'color') {
      i.type = 'color'
      i.addEventListener('input', function(ev) {
        if (ev.target.getAttribute('type') === 'color')
          object[field] = ev.target.value } ) }

    else if (typeof object[field] === 'boolean') {
      i.type = 'checkbox'
      i.checked = object[field]

      i.addEventListener('change', function(ev) {
        console.log('on change', ev.target, ev.target.checked)
        object[field] = ev.target.checked } ) }

    else if (typeof object[field] === 'string') {
      i.addEventListener('keydown', function(ev) {
        if (ev.key == 'Enter') {
          var value = c.tryEval(ev.target.value)
          if (typeof value !== 'undefined') {
            object[field] = value
            e.update() } } }) }

    else if (typeof object[field] === 'number') {
      i.addEventListener('click', function(ev) {
        i.value = object[field] })

      i.addEventListener('keydown', function(ev) {
        console.log('keydown', ev.key)

        if (ev.key == 'Enter') {
          var value = c.tryEval(ev.target.value)
          if (typeof value !== 'undefined') {
            object[field] = value
            e.update() } }

        else {
          var value = c.tryEval(ev.target.value + ev.key)
          if (ev.ctrlKey && ev.altKey) delta = 1000
          else if (ev.ctrlKey) delta = 10
          else if (ev.altKey) delta = 100
          else delta = 1

          if (ev.key == 'ArrowUp') {
            object[field] = (c.tryEval(ev.target.value) || 0) + delta
            e.update() }

          else if (ev.key == 'ArrowDown') {
            object[field] = (c.tryEval(ev.target.value) || 0) - delta
            e.update() }

          if (ev.key == 'ArrowRight') {
            object[field] = object[field] + delta
            e.update() }

          else if (ev.key == 'ArrowLeft') {
            object[field] = object[field] - delta
            e.update() }

          else {
            // TODO skip?
            //console.log('what to do?')
            //object[field] = current || 0
          } } }) }

    else {
      console.warn('cannot create knob: unknown field type', typeof object[field], object, field) }

    e.update = function() {
      i.value = object[field] }

    return e }

  c.getKnobs = function(thing, title) {
    var e = document.createElement('section')
    e.thing = thing
    e.style.display = 'inline-block'
    e.style['vertical-align'] = 'top'
    e.title = thing._id

    var header = document.createElement('header')
    header.innerText = title || 'some thing'
    var close = document.createElement('button')
    close.addEventListener('click', function(ev) {
      c.remove(thing._id)
      e.remove() })

    close.innerText = 'X'
    header.appendChild(close)

    var id = document.createElement('small')
    id.innerText = thing._id
    header.appendChild(id)

    e.appendChild(header)

    var keys = Object.keys(thing)
    for (f in keys) {
      if (keys[f][0] != '_') {
        var knobElement = c.getKnob(keys[f], thing, keys[f])
        knobElement.style.display = 'block'
        e.appendChild(knobElement) } }

    return e }

  c.init = function(window, element, width, height) {
    c.window = window

    if (typeof element === 'undefined') {
      c.element = window.document.createElement('canvas')
      window.document.body.appendChild(c.element) }

    else c.element = element

    c.window.addEventListener('keydown', function(ev) {
      if (typeof c.keydown[ev.key] === 'function') {
        ev.preventDefault()
        ev.stopPropagation()
        c.keydown[ev.key](ev)
        return false } })

    c.window.addEventListener('keyup', function(ev) {
      if (typeof c.keyup[ev.key] === 'function') {
        ev.preventDefault()
        ev.stopPropagation()
        c.keyup[ev.key]()
        return false } })

    //c.element.addEventListener('click', function(ev) {
      //c.togglePlay() })

    c.mouse = {
      x : 10,
      y : 10,
      drag : [],
      dragging : false,
      entered : undefined,
      left : undefined }

    c.element.addEventListener('mouseenter', function(ev) {
      c.mouse.entered = ev } )

    c.element.addEventListener('mousedown', function(ev) {
      c.mouse.dragging = true
      c.mouse.drag = [ { x : ev.layerX, y : ev.layerY } ] })

    c.element.addEventListener('mousemove', function(ev) {
      //console.log('mouse pos', ev.layerY, ev.screenY, ev.layerY, ev.clientY)
      if (c.mouse.dragging) {
        c.mouse.drag.push({ x : ev.layerX, y : ev.layerY })
        var l = c.mouse.drag.length
        if (ev.ctrlKey) {
          if (Math.abs(c.mouse.drag[0].x - c.mouse.drag[l-1].x) > Math.abs(c.mouse.drag[0].y - c.mouse.drag[l-1].y)) {
            c.mouse.drag[l-1].y = c.mouse.drag[0].y }

          else {
            c.mouse.drag[l-1].x = c.mouse.drag[0].x } }

        c.line(c.mouse.drag[0].x, c.mouse.drag[0].y, c.mouse.drag[l-1].x, c.mouse.drag[l-1].y, 'cyan') } })

    c.element.addEventListener('mouseleave', function(ev) {
      c.mouse.left = ev } )

    c.element.addEventListener('mouseup', function(ev) {
      c.mouse.dragging = false

      if (c.mouse.drag.length == 1) {
        c.togglePlay() }

      else if (c.mouse.drag.length == 0) {
        console.log('dragged something onto canvas?') }

      else {
        var line = { x1 : c.mouse.drag[0].x,
          y1 : c.mouse.drag[0].y,
          x2 : c.mouse.drag[c.mouse.drag.length-1].x,
          y2 : c.mouse.drag[c.mouse.drag.length-1].y  }

        if (ev.altKey) {
          line.x1d = 0
          line.y1d = 0
          line.x2d = 0
          line.y2d = 0 }

        c.create('line', line) } })

    element.width = element.style.width = width || 640
    element.width = element.style.height = height || 480
    c.auto()
    c.context2d = element.getContext('2d') }

  c.initGui = function() {
    if (c.element.gui) {
      console.warn('removing old gui from hud')
      c.element.gui.remove() }

    var gui = { }

    gui.add = document.createElement('button')
    gui.add.innerText = '+'
    gui.add.style.color = 'green'
    gui.add.addEventListener('click', function(ev) { c.create('line', {}) })

    gui.clear = document.createElement('button')
    gui.clear.innerText = 'Clear'
    gui.clear.style.color = 'gray'
    gui.clear.addEventListener('click', function(ev) { c.clear() })

    gui.stop = document.createElement('button')
    gui.stop.innerText = 'Stop'
    gui.stop.style.color = 'red'
    gui.stop.addEventListener('click', function(ev) { c.stop() })

    gui.play = document.createElement('button')
    gui.play.innerText = 'Play'
    gui.play.style.color = 'green'
    gui.play.addEventListener('click', function(ev) { c.play() })

    gui.width = c.getKnob('width', c.element, 'width')
    gui.height = c.getKnob('height', c.element, 'height')

    gui.fit = document.createElement('button')
    gui.fit.style.color = 'orange'
    gui.fit.innerText = 'fit'
    gui.fit.addEventListener('click', function(ev) { c.fit() })

    gui.auto = document.createElement('button')
    gui.auto.style.color = 'orange'
    gui.auto.innerText = 'auto'
    gui.auto.addEventListener('click', function(ev) { c.auto() })

    gui.sharp = document.createElement('button')
    gui.sharp.innerText = '1:1'
    gui.sharp.style.color = 'orange'
    gui.sharp.addEventListener('click', function(ev) { c.canvasToRes() })

    gui.resToCanvas = document.createElement('button')
    gui.resToCanvas.innerText = 'real'
    gui.resToCanvas.style.color = 'orange'
    gui.resToCanvas.addEventListener('click', function(ev) { c.resToCanvas() })

    gui.snapshot = document.createElement('button')
    gui.snapshot.innerText = 'Snapshot'
    gui.snapshot.style.color = 'yellow'
    gui.snapshot.addEventListener('click', function(ev) {
      var thumb = document.createElement('section')
      thumb.style.display = 'inline-block'
      var img = document.createElement('img')
      thumb.appendChild(img)
      var data = c.element.toDataURL('image/png')
      var meta = document.createElement('p')
      meta.innerText = c.element.width + 'x' + c.element.height
      thumb.appendChild(meta)
      guiElement.appendChild(thumb)
      img.src = data
      img.style.background = 'white'
      img.width = 125
      img.height = 125 })

    gui['r1080'] = document.createElement('button')
    gui['r1080'].innerText = '1920v1080'
    gui['r1080'].addEventListener('click', function(ev) {
      c.setResolution(1920, 1080) })

    gui['r720'] = document.createElement('button')
    gui['r720'].innerText = '1280x720'
    gui['r720'].addEventListener('click', function(ev) {
      c.setResolution(1280, 720) })

    gui['r480'] = document.createElement('button')
    gui['r480'].innerText = '640x480'
    gui['r480'].addEventListener('click', function(ev) {
      c.setResolution(640, 480) })

    gui['r250'] = document.createElement('button')
    gui['r250'].innerText = '250x250'
    gui['r250'].addEventListener('click', function(ev) {
      c.setResolution(250, 250) })

    gui['r100'] = document.createElement('button')
    gui['r100'].innerText = '100x100'
    gui['r100'].addEventListener('click', function(ev) {
      c.setResolution(100, 100) })

    gui.end = document.createElement('hr')

    var guiElement = document.createElement('section')
    guiElement.style.textAlign = 'center'

    var k = Object.keys(gui)
    for (i in k)
      guiElement.appendChild(gui[k[i]])

    c.element.parentElement.insertBefore(guiElement, c.element.nextSibling) }

  c.keydown = {
    'k' : function() {
      c.togglePlay() },

    'l' : function() {
      c.frame(++c.frameCount, c.frameCount/60) },

    ' ' : function(ev) {
      c.create('line') } }

  c.keyup = { }

  c.line = function(x1, y1, x2, y2, style) {
    c.context2d.beginPath()
    c.context2d.moveTo(x1, y1)
    c.context2d.lineTo(x2, y2)
    c.context2d.strokeStyle = style
    c.context2d.stroke() }

  c.loop = function(time) {
    if (!c.playing) return
    c.frameTimeLast = c.frameTime
    c.frameTime = time
    c.frameTimeDelta = c.frameTime - c.frameTimeLast
    c.frame(++c.frameCount, c.frameCount/60, time, c.frameTimeDelta) // ! assuming 60fps (probably not wise)
    c.window.requestAnimationFrame(c.loop) }

  c.nextId = 0

  c.pipe = function(pipe, slask) {
    console.log('slask', slask)
    for (p in c.pipes[pipe]) {
      c.pipes[pipe][p].apply(c.pipes[pipe][p].context || c, slask) } }

  c.pipes = {
    update : [],
    draw : [],
    hud : [] }

  c.pixel = function(x, y) {
    // concider http://stackoverflow.com/questions/4899799/whats-the-best-way-to-set-a-single-pixel-in-an-html5-canvas
    c.context2d.fillRect(x, y, 1, 1) }

  c.play = function() {
    if (c.playing) return
    c.playing = true
    c.frameTime = new Date()
    c.frameTimeLast = c.frameTime
    c.window.requestAnimationFrame(c.loop) }

  c.remove = function(id) {
    console.log('delete', id)
    var thing = c.things[id]
    c.disablePipes(thing)
    delete(c.things[id])
    console.debug('deleted', thing)
    return thing }
  
  // c.record = function(command, buffer) { }

  c.resToCanvas = function() {
    var r = c.element.getBoundingClientRect()
    c.element.style.width = c.element.width + 'px'
    c.element.style.height = c.element.height + 'px' }

  c.setResolution = function(w, h) {
    console.call(arguments)
    c.element.width = w
    c.element.height = h }

  c.stop = function() {
    c.playing = false }

  c.text = function(text, x, y, font, fill, stroke) {
    c.context2d.font = font || '2rem monospace'
    c.context2d.strokeStyle = stroke || 'black'
    c.context2d.lineWidth = 5
    c.context2d.fillStyle = fill || 'white'
    c.context2d.textBaseline = 'hanging'
    c.context2d.strokeText(text, x, y)
    c.context2d.fillText(text, x, y) }

  c.things = {}

  c.togglePlay = function() {
    if (c.playing) 
      c.stop()
    else
      c.play() }

  c.tryEval = function(i) {
    var o = undefined

    try {
      o = eval(i) }

    catch (e) {
      c.feedback('info', 'hej', 'hopp', 1,2,4 [5,6,7]) }

    finally {
      return o } }

  return c }

Canvas.iterator = function(palette, nexter, env) {
  var current = 0
  env = env || { d : 1 } // TODO proper
  var iterate = function() {
    var next = nexter(palette, current, env)
    current = next
    return palette[next] }

  return iterate }

Canvas.nexters = { }

Canvas.nexters.linear = function(p, c) {
  var n = c + 1
  if (n >= p.length) n = 0
  return n }

Canvas.nexters.linearBounce = function(p, c, env) {
  var n = c + env.d
  if (n >= p.length || n < 0) {
    env.d *= -1
    n = c + env.d }

  return n }

Canvas.palettes = { };

Canvas.palettes.black = function(step) {
  return [ '#000000' ] }

Canvas.palettes.gray = function(step) {
  step = step || 0x0F
  var a = []
  for (var i = 0x00 ; i <= 0xFF ; i+=step ) {
    a.push('#' + i.toString(16) + i.toString(16) + i.toString(16)) }

  return a }(0x20);

Canvas.palettes.rgb = [
  '#FF0000',
  '#00FF00',
  '#0000FF' ]

Canvas.palettes.rygcbm = [
  '#FF0000',
  '#FFFF00',
  '#00FF00',
  '#00FFFF',
  '#0000FF',
  '#FF00FF' ]

Canvas.palettes.pink = [
  'pink',
  'lightpink',
  'hotpink',
  'deeppink',
  'palevioletred',
  'mediumvioletred' ]

Canvas.palettes.web = [
  'aqua',
  'black',
  'blue',
  'fuchsia',
  'gray',
  'green',
  'line',
  'maroon',
  'navy',
  'olive',
  'purple',
  'red',
  'silver',
  'teal',
  'white',
  'yellow' ]

Canvas.randomColor = function(a) {
  var r = Math.floor(Math.random() * 255)
  var g = Math.floor(Math.random() * 255)
  var b = Math.floor(Math.random() * 255)
  return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + (a || Math.random()) + ')' }

Canvas.randomField = function(obj) {
  var keys = Object.keys(obj)
  var ri = Canvas.randomInt(keys.length)
  return obj[keys[ri]] }

Canvas.randomInt = function(size) {
  size = size || 100
  return Math.floor(Math.random() * size) }

Canvas.styleToRGBA = function(style) {
  return  }

Canvas.types = { }

Canvas.types.countable = function(t) {
  t = Canvas.types.thing(t)
  t._updateCount = 0
  return t }

Canvas.types.colorable = function(t) {
  t.red = 100
  t.green = 100
  t.blue = 100
  t.alpha = 100
  return Canvas.types.countable(t) }

Canvas.types.palettable = function(t) {
  t.palette = t.palette || Canvas.randomField(Canvas.palettes)
  t._nextInPalette = t._nexter || Canvas.nexters.linear
  t._nextColor = Canvas.iterator(t.palette, t._nextInPalette)
  t.color = t.palette[t.palette.length - 1]
  return Canvas.types.countable(t) }

Canvas.types.ball = function(t) {
  c = this
  if (typeof t.x === 'undefined') t.x = Canvas.randomInt(100)
  if (typeof t.y === 'undefined') t.y = Canvas.randomInt(100)
  if (typeof t.r === 'undefined') t.r = Canvas.randomInt(10)
  if (typeof t.xd === 'undefined') t.xd = Canvas.randomInt(10) - 5
  if (typeof t.yd === 'undefined') t.yd = Canvas.randomInt(10) - 5
  if (typeof t.fill === 'undefined') t.fill = Canvas.randomColor(0.7)
  if (typeof t._frames === 'undefined') t._frames = 0
  if (typeof t.maxFrames === 'undefined') t.maxFrames = 60*60
  if (typeof t.stroke === 'undefined') t.stroke = Canvas.randomColor(0.3)
  if (typeof t._update === 'undefined') {
    t._update = function(frame, time) {
      console.log('update ball')
      if (t.x < 0) t.xd++
      else if (t.x >= c.element.width) t.xd--
      else t.xd *= 0.99

      t.xd += (Math.random() - .5)/10

      if (t.y < 0) t.yd++
      else if (t.y >= c.element.height) t.yd--
      else t.yd *= 0.99

      t.yd += (Math.random() - .5)/10

      t.x += t.xd
      t.y += t.yd
   
      if (++t._frames > t.maxFrames) {
        c.remove(t._id) }
    } }

  if (typeof t._draw === 'undefined') {
    t._draw = function(frame, time) {
      c.context2d.fillStyle = t.fill
      c.context2d.strokeStyle = t.stroke
      c.context2d.lineWidth = t.r/10
      //console.log('r, t.stroke, t.fill', t.r, t.stroke, t.fill)
      c.context2d.beginPath()
      c.context2d.arc(t.x, t.y, t.r, 0, Math.TAU)
      c.context2d.fill()
      c.context2d.save()
      c.context2d.globalAlpha = 0.2
      //c.context2d.beginPath()
      //c.context2d.arc(t.x, t.y, t.r + 2, 0, Math.TAU, false)
      c.context2d.stroke()

      c.context2d.beginPath()
      //c.context2d.moveTo(t.x, t.y)
      c.context2d.moveTo(t.x, t.y)
      c.context2d.bezierCurveTo(t._hit.l1.x1, t._hit.l1.y1, t._hit.l2.x2, t._hit.l2.y2, t.x, t.y)
      //c.context2d.bezierCurveTo(t._hit.l1.x1, t._hit.l2.y1, t._hit.x, t._hit.y, t.x, t.y)
      c.context2d.strokeStyle = t._hit.l1.color
      c.context2d.stroke()
      c.context2d.restore() } }

  t = Canvas.types.palettable(t)

  return t }

Canvas.types.line = function(t) {
  c = this
  t = Canvas.types.palettable(t)

  t['colorSwitch%'] = (typeof t['colorSwitch%']) !== 'undefined'
    ? t['colorSwitch%']
    : Canvas.randomInt(3)

  t.linesPerFrame = (typeof t.linesPerFrame) !== 'undefined'
    ? t.linesPerFrame
    : Canvas.randomInt(10) + 1

  //t.color = (typeof t.color) !== 'undefined'
    //? t.color
    //: 'cyan'

  t.collidable = t.collidable || true

  t.x1 = (typeof t.x1) !== 'undefined'
    ? t.x1
    : Canvas.randomInt(c.element.width)

  t.y1 = (typeof t.y1) !== 'undefined'
    ? t.y1
    : Canvas.randomInt(c.element.height)

  t.x2 = (typeof t.x2) !== 'undefined'
    ? t.x2
    : Canvas.randomInt(c.element.width)

  t.y2 = (typeof t.y2) !== 'undefined'
    ? t.y2
    : Canvas.randomInt(c.element.height)

  t.x1d = (typeof t.x1d) !== 'undefined'
    ? t.x1d
    : Canvas.randomInt(7) - 4

  t.y1d = (typeof t.y1d) !== 'undefined'
    ? t.y1d
    : Canvas.randomInt(7) - 4

  t.x2d = (typeof t.x2d) !== 'undefined'
    ? t.x2d
    : Canvas.randomInt(7) - 4

  t.y2d = (typeof t.y2d) !== 'undefined'
    ? t.y2d
    : Canvas.randomInt(7) - 4
  //t.boundaryCheck = t.y2d || Canvas.randomInt(10) - 5
  //t.minLength = t.minLength || 10
  //t.maxLength = t.maxLength || 500

  var knobs = t._knobs = c.getKnobs(t, 'line ' + t._id)
  c.element.parentElement.appendChild(knobs)

  t._iterate = function() {
    for (i = 0 ; i < t.linesPerFrame ; i++) {
      t._update()
      t._draw() } }

  t._draw = function() {
    c.line(t.x1, t.y1, t.x2, t.y2, t.color) }

  t._update = function() {
    if (++t._updateCount % t['colorSwitch%'] == 0) {
      t.color = t._nextColor() }

    t.x1 += t.x1d
    t.y1 += t.y1d
    t.x2 += t.x2d
    t.y2 += t.y2d

    // boundary check

    // bounce (and nudge inside)
    if (t.x1 >= c.element.width) {
      t.x1d *= -1
      t.x1 = c.element.width -1 }

    if (t.x1 < 0) {
      t.x1d *= -1
      t.x1 = 0 }

    if (t.x2 >= c.element.width) {
      t.x2d *= -1
      t.x2 = c.element.width - 1 }

    if (t.x2 < 0) {
      t.x2d *= -1
      t.x2 = 0 }

    if (t.y1 >= c.element.height) {
      t.y1d *= -1
      t.y1 = c.element.height - 1 }

    if (t.y1 < 0) {
      t.y1d *= -1
      t.y1 = 0 }

    if (t.y2 >= c.element.height) {
      t.y2d *= -1
      t.y2 = c.element.height - 1 }

    if (t.y2 < 0) {
      t.y2d *= -1
      t.y2 = 0 }

    // wrap other side
    //if (t.x1 >= c.element.width) t.x1 = 0
    //if (t.x1 < 0) t.x1 = c.element.width
    //if (t.y1 >= c.element.height) t.y1 = 0
    //if (t.y1 < 0) t.y1 = c.element.height
    //if (t.x2 >= c.element.width) t.x2 = 0
    //if (t.x2 < 0) t.x2 = c.element.width
    //if (t.y2 >= c.element.height) t.y2 = 0
    //if (t.y2 < 0) t.y2 = c.element.height
  }

  return t }

Canvas.types.thing = function(t) {
  t = t || { }
  t._update = t._update || function() { }
  t._draw = t._draw || function() { }

  t._iterate = t._iterate || function(frame, time) {
    this._update(frame, time)
    this._draw(frame, time) }

  return t }
