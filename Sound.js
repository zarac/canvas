var Sound = { }

Sound.context = new AudioContext()
Sound.output = Sound.context.createGain()
Sound.output.connect(Sound.context.destination)

Sound.cache = { }

Sound.cache.o1 = new OscillatorNode(Sound.context)

Sound.loadBuffer = function(url, done) {
  var request = new XMLHttpRequest()
  request.open('GET', url, true)
  request.responseType = 'arraybuffer'

  request.onload = function() {
    Sound.context.decodeAudioData(
      request.response,
      function(buffer) {
        done(null, buffer) },

      function(err) {
        done(err) } ) }

    request.send() }

Sound.play = function(url, rate) {
  if (typeof Sound.cache[url] !== 'undefined')
    Sound.playBuffer(Sound.cache[url], rate)

  else {
    Sound.loadBuffer(url, function(err, buffer) {
      if (err) {
        console.log('could not load sound file', url) }

      else {
        Sound.cache[url] = buffer
        Sound.playBuffer(Sound.cache[url], rate) } }) } }

Sound.playBuffer = function(buffer, rate) {
  var s = Sound.context.createBufferSource(buffer)
  s.buffer = buffer
  s.playbackRate.value = rate || 1
  s.connect(Sound.output)
  s.start(0) }

Sound.toggleMute = function() {
  console.log('dest', Sound.output, Sound.output.numberOfOutputs, Sound.output.destination)

  if (Sound.output.numberOfOutputs == 0) {
		console.log('do unmute')
    Sound.output.connect(Sound.context.destination) }

  else {
		console.log('do mute')
    Sound.output.disconnect() } }

Sound.playOscillatorTypes = ['sine', 'square', 'sawtooth', 'triangle']

Sound.playOscillator = function(type, frequency, detune, length) {
	var o = Sound.context.createOscillator()
  o.type = type || 'sine'
  o.frequency.value = frequency || 440
  o.detune.value = detune || 100
	o.start()
  if (typeof length !== 'undefined') {
    o.connect(Sound.output)
    window.setTimeout(function() {
      o.disconnect(Sound.output) },
      length) }

  return o }
