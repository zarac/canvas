var c = window.c = Canvas()

document.addEventListener('readystatechange', function() {
  if (document.readyState === 'interactive') {
    var e = document.querySelector('body > canvas')

    c.init(window, e)
    c.play()
    c.initGui()

    Sound.play('kick.wav')
    //Sound.playOscillator('square', 440)
    c.keydown['m'] = Sound.toggleMute

    Sound.playOscillator('square', 340, 1000, 100)
    Sound.playOscillator('square', 240, 1000, 300)
    Sound.playOscillator('square', 140, 1000, 500)

    var s = Sound.playOscillator('sine', 440, 1000)
    c.keydown['b'] = function() {
      console.log('time', Sound.context.currentTime)
      s.frequency.value = 440
      s.frequency.linearRampToValueAtTime(540, Sound.context.currentTime + 0.1)
      s.connect(Sound.output) }
    c.keyup['b'] = function() { s.disconnect(Sound.output) }
    console.log('s', s)
    c.element.parentElement.appendChild(c.getKnobs(s, 'sine'))

    c.pixel(10, 10, 'black')
    c.pixel(11, 11, 'black')
    c.pixel(12, 12, 'black')
    c.pixel(13, 13, 'black')
    c.pixel(14, 14, 'black')
    c.pixel(15, 15, 'black')
    c.pixel(16, 16, 'black')
    c.pixel(17, 17, 'black')
    c.pixel(18, 18, 'black')
    c.pixel(19, 19, 'black')
    c.pixel(20, 20, 'black')
    c.pixel(21, 21, 'black')

    var hud = c.create('hud', {
      enabled : true,
      _hud : function(frame, time, delta) {
        if (this.enabled)
          c.text(Object.keys(c.things).length + ' things\n' + frame + ' frames ' + time + ' time ' + delta + ' delta', 10, 10) } })
    
    c.element.parentElement.appendChild(c.getKnobs(hud, 'HUD'))

    var t = c.create('clear', {
      frame : 6,
      burst : 3,
      fixedColor : true,
      color : Canvas.randomColor(),
      alpha : 0.1,
      _draw : function(frame, time) {
        if ((time * 100) % this.frame < this.burst) {
          var oldAlpha = c.context2d.globalAlpha
          //c.context2d.save()
          c.context2d.globalAlpha = t.alpha
          c.context2d.fillStyle = (t.fixedColor && t.color) || Canvas.randomColor()
          c.context2d.fillRect(0, 0, c.element.width, c.element.height)
          c.context2d.globalAlpha = oldAlpha
          //c.context2d.restore()
        } } } )
    
    c.element.parentElement.appendChild(c.getKnobs(t, 'clear'))

    //c.create('line', {
      //x1 : c.element.width/2,
      //y1 : 10,
      //x2 : c.element.width/2 - 10,
      //y2 : c.element.height * .7,
      //x1d : 0,
      //x2d : -1,
      //y1d : 0,
      //y2d : 0,
      //palette : Canvas.palettes.pink,
      //'colorSwitch%' : 7,
      //linesPerFrame : 13 })

    //c.create('line', { x1 : 0, y1 : 0, x1d : 0, y1d : 0, x2d : 1, y2d : 1 })

    //c.create('line',
      //{ x1 : 0, y1 : 0,
        //x2 : 0, y2 : c.element.height * .1,
        //x1d : 1, y1d : 1,
        //x2d : 1, y2d : 0,
        //'colorSwitch%' : 1,
        //palette : Canvas.palettes.rygcbm })

    //c.create('line', { x1 : 0, y1 : 0, x2 : 0, y2 : c.element.height, x1d : 1, y1d : 0, x2d : 1, y2d : 0 })

    /*
    c.create('line', {})

    c.create(
      'line',
      { 'colorSwitch%' : 60,
         linesPerFrame : 1,
         x1d : -1,
         x2d : -3,
         y1d : -2,
         y2d : -2,
         palette : Canvas.palettes.rgb,
         _nexter : Canvas.nexters.linearBounce } )

    c.create(
      'line',
      { 'colorSwitch%' : 1,
         linesPerFrame : 0.01,
         x1d : -1,
         x2d : -1,
         y1d : -2,
         y2d : -2,
         palette : Canvas.palettes.pink,
         _nexter : Canvas.nexters.linearBounce } )

    c.create(
      'line',
      { 'colorSwitch%' : 1,
         linesPerFrame : 1,
         x1d : -1,
         x2d : 1,
         y1d : -2,
         y2d : -3,
         palette : Canvas.palettes.gray,
         _nexter : Canvas.nexters.linearBounce } )
    */

    c.create(
      'ball-collision-checker',
      { _update : function() {
        this.hits = [] } } )

    c.create(
      'line-collision-checker',
      { _update : function() {
        this.hits = []
        var ids = Object.keys(c.things)
        var length = ids.length
        for (var l1i = 0 ; l1i < length ; l1i++) {
          var l1 = c.things[ids[l1i]]
          if (!l1.collidable) continue
          for (var l2i = l1i + 1 ; l2i < length ; l2i++) {
            var l2 = c.things[ids[l2i]]
            if (!l2.collidable) continue

            var X = segmentIntersect(
              { x : l1.x1, y : l1.y1 },
              { x : l1.x2, y : l1.y2 },
              { x : l2.x1, y : l2.y1 },
              { x :l2.x2, y : l2.y2 } )

            if (!X) continue

            // thanks https://www.youtube.com/watch?v=A86COO8KC58?t=7m17s
            // Ax + Bx = C

            /*
            var A1 = l1.y2 - l1.y1
            var B1 = l1.x2 - l1.x1
            var C1 = (A1 * l1.x1) + (B1 * l1.y1)
            console.log('l1', A1, B1, C1) 
            var A2 = l2.y2 - l2.y1
            var B2 = l2.x2 - l2.x1
            var C2 = (A2 * l2.x1) + (B2 * l2.y1)
            console.log('l2', A2, B2, C2) 
            var denominator = (A1 * B2) - (A2 * B1)
            if (denominator == 0) continue
            */

            /*
            if (X) {
              c.context2d.strokeStyle = 'cyan'
              c.context2d.fillStyle = 'magenta'
              c.context2d.arc(X.x, X.y, 10, 0, Math.TAU, false)
              c.context2d.fill()
              c.context2d.stroke() }

            l1.A = l1.y2 - l1.y1
            l1.B = l1.x2 - l1.x1
            l1.C = (l1.A * l1.x1) + (l1.B * l1.y1)
            console.log('l1', l1.A, l1.B, l1.C) 
            l2.A = l2.y2 - l2.y1
            l2.B = l2.x2 - l2.x1
            l2.C = (l2.A * l2.x1) + (l2.B * l2.x1)
            console.log('l2', l2.A, l2.B, l2.C) 
            var denominator = (l1.A * l2.B) - (l2.A * l1.B)
            if (denominator == 0) continue

            var x = ((l2.B * l1.C) - (l1.B * l2.C)) / denominator
            var y = ((l1.A * l2.C) - (l2.A * l1.C)) / denominator
            //var y = ((l2.A * l1.C) - (l1.A * l2.C)) / denominator
            console.log('intersect', x, y)
            var l1rx = (x - l1.x1) / (l1.x2 - l1.x1)
            var l1ry = (y - l1.y1) / (l1.y2 - l1.y1)
            var l2rx = (x - l2.x1) / (l2.x2 - l2.x1)
            var l2ry = (y - l2.y1) / (l2.y2 - l2.y1)
            console.log('ratio', l1rx, l1ry, l2rx, l2ry)
            //console.log('denominator', denominator, x, y, ratioX1, l1ry, l2rx, l2ry)
            //if (!(((ratioX1 => 0 && ratioX1 <= 1) || (l1ry => 0 && l1ry <= 1))
            //|| ((l2rx => 0 && l2rx <= 1) || (l2ry => 0 && l2ry <= 1))))
              //continue

            c.context2d.beginPath()
            c.context2d.arc(x, y, 13, 0, Math.TAU, false)
            c.context2d.fillStyle = 'lightgray'
            c.context2d.fill()
            c.context2d.strokeStyle = 'cyan'
            c.context2d.stroke()

            c.line(x, y, c.element.width/2, c.element.height/2, 'rgba(155, 255, 155, 0.5)')

            if (!((l1rx <= 0 && l1rx <= 1) || (l1ry <= 0 && l1ry <= 1)) ) {
              console.log('outside l1')
              c.line(x, y, l1.x1, y, 'rgba(0, 255, 255, 0.5)')
              continue }

            if (!((l2rx <= 0 && l2rx <= 1) || (l2ry <= 0 && l2ry <= 1)) ) {
              console.log('outside l2')
              c.line(x, y, l2.x1, y, 'rgba(255, 0, 255, 0.5)')
              continue }
            */

            //Sound.play('hihat.wav', (X.y / c.element.height))
            Sound.playOscillator('square', c.element.height - X.y, 100, 100)

            var hit = {
              l1 : l1,
              l2 : l2,
              //l1ry : denominator,
              //l1rx : l1rx,
              //l1ry : l1ry,
              //l2rx : l2rx,
              //l2ry : l2ry,
              x : X.x,
              y : X.y }

            this.hits.push(hit)

            var l1rx = (2 - (Math.abs(l1.x1 - l1.x2) / c.element.width + Math.abs(l1.x1 - l1.x2) / c.element.width))/2
            var l1ry = (2 - (Math.abs(l1.y1 - l1.y2) / c.element.height + Math.abs(l1.y1 - l1.y2) / c.element.height))/2
            var l2rx = (2 - (Math.abs(l2.x1 - l2.x2) / c.element.width + Math.abs(l2.x1 - l2.x2) / c.element.width))/2
            var l2ry = (2 - (Math.abs(l2.y1 - l2.y2) / c.element.height + Math.abs(l2.y1 - l2.y2) / c.element.height))/2

            c.create('ball', {
              x : X.x,
              y : X.y,
              r : l1rx * l1rx * c.element.width / 100,
              //xd : (l2.x2d - l2.x1d) - (l1.x2d - l1.x1d),
              //xd : Math.abs(l1.x1d - l2.x2d) + Math.abs(l2.x1d - l2.x2d),
              //yd : (l2.y2d - l2.y1d) - (l1.y2d - l1.y1d),
              xd : l1rx * l1rx * c.element.width / 10,
              yd : l1ry * l1ry * c.element.height / 10,
              maxTime : 60,
              fill : l1.color,
              stroke : l2.color,
              _hit : hit }) } } },

      _draw : function() {
        c.context2d.fillStyle = 'tomato'
        c.context2d.strokeStyle = 'cyan'
        for (var i = 0 ; i < this.hits.length ; i++ ) {
          var hit = this.hits[i]
          c.context2d.beginPath()
          c.context2d.arc(hit.x, hit.y, 10, 0, Math.TAU, false)
          c.context2d.fill()
          c.context2d.stroke()

          c.context2d.strokeStyle = 'rgba(0, 155, 0, 0.2)'
          c.context2d.beginPath()
          c.context2d.moveTo(hit.x, hit.y)
          c.context2d.lineTo(0, 0)
          c.context2d.stroke()

          c.context2d.beginPath()
          c.context2d.moveTo(hit.x, hit.y)
          c.context2d.lineTo(c.element.width, 0)
          c.context2d.stroke()

          c.context2d.beginPath()
          c.context2d.moveTo(hit.x, hit.y)
          c.context2d.lineTo(0, c.element.height)
          c.context2d.stroke()

          c.context2d.beginPath()
          c.context2d.moveTo(hit.x, hit.y)
          c.context2d.lineTo(c.element.width, c.element.height)
          c.context2d.stroke() } } } ) } })
